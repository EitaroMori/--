package com.example.tksgakr.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends Activity {

    //音声出力用のクラス
    SoundPlayer sp;
    //アプリで使用する画面領域の幅と高さ
    double contentWidth, contentHeight;
    //座標領域を判定する際に基準となる幅と高さの数値
    double x_basicUnit, y_basicUnit;
    //横と縦の座標領域
    double x_cordinateRange, y_cordinateRange;
    //タッチorフリックをした際の座標と距離、経過時間
    float start_x, start_y, distance_x, distance_y,
            distance_both, finish_x, finish_y, move_x, move_y, elapsedTime;
    //音声出力の際に用いる、再生速度と音量
    float rate, soundVolume;
    //タッチorフリックをしたor終えた際の時間。TextViewを最大化した際の時間
    long startTime, finishTime, maximizeTextViewWidthTime;
    //画面の向きを判定する変数。デバッグモードが選択されているか否かを判定する変数。ステータスバーの高さ
    int vertical_or_horizontal, dbgtxt_vis_or_unvis, statusBarHeight;
    int sound_id = -1;
    //デバッグモード用のTextView
    TextView dbgtxt;
    //背景設定用のTextView
    TextView txt_DO, txt_RE, txt_MI, txt_FA, txt_SO, txt_RA, txt_SHI;
    LinearLayout linearLayout;
    Configuration config;

    //アプリ起動時に初めに実行される処理を設定する
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp = new SoundPlayer(this);
        config = getResources().getConfiguration();
        txt_DO = (TextView)findViewById(R.id.DO);
        txt_RE = (TextView)findViewById(R.id.RE);
        txt_MI = (TextView)findViewById(R.id.MI);
        txt_FA = (TextView)findViewById(R.id.FA);
        txt_SO = (TextView)findViewById(R.id.SO);
        txt_RA = (TextView)findViewById(R.id.RA);
        txt_SHI = (TextView)findViewById(R.id.SHI);
    }

    //ステータスバーの高さを取得する
    public static int getStatusBarHeight(Activity activity) {
        final Rect rect = new Rect();
        Window window = activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    /*
    //背景画像の配置を決める
    public void placeBackgroundColorByCoordinate(){
        txt_DO = (TextView)findViewById(R.id.DO);
        txt_RE = (TextView)findViewById(R.id.RE);
        txt_MI = (TextView)findViewById(R.id.MI);
        txt_FA = (TextView)findViewById(R.id.FA);
        txt_SO = (TextView)findViewById(R.id.SO);
        txt_RA = (TextView)findViewById(R.id.RA);
        txt_SHI = (TextView)findViewById(R.id.SHI);

        if(vertical_or_horizontal == 1){
            txt_DO.setWidth((int) x_basicUnit);
            txt_RE.setWidth((int) x_basicUnit);
            txt_MI.setWidth((int) x_basicUnit);
            txt_FA.setWidth((int) x_basicUnit);
            txt_SO.setWidth((int) x_basicUnit);
            txt_RA.setWidth((int) x_basicUnit);
            txt_SHI.setWidth((int) x_basicUnit);
            txt_DO.setScaleY((float) y_cordinateRange);
            txt_RE.setScaleY((float) y_cordinateRange);
            txt_MI.setScaleY((float) y_cordinateRange);
            txt_FA.setScaleY((float) y_cordinateRange);
            txt_SO.setScaleY((float) y_cordinateRange);
            txt_RA.setScaleY((float) y_cordinateRange);
            txt_SHI.setScaleY((float) y_cordinateRange);

            //TextViewを配置する座標を決める
            txt_DO.setTranslationX((float)x_basicUnit * 1);
            txt_RE.setTranslationX((float)x_basicUnit * 2);
            txt_MI.setTranslationX((float)x_basicUnit * 3);
            txt_FA.setTranslationX((float)x_basicUnit * 4);
            txt_SO.setTranslationX((float)x_basicUnit * 5);
            txt_RA.setTranslationX((float)x_basicUnit * 6);
            txt_SHI.setTranslationX((float)x_basicUnit * 7);

        } else {
            txt_DO.setScaleX((float) x_basicUnit);
            txt_RE.setScaleX((float)x_basicUnit);
            txt_MI.setScaleX((float)x_basicUnit);
            txt_FA.setScaleX((float)x_basicUnit);
            txt_SO.setScaleX((float)x_basicUnit);
            txt_RA.setScaleX((float)x_basicUnit);
            txt_SHI.setScaleX((float) x_basicUnit);
            txt_DO.setScaleY((float) y_cordinateRange);
            txt_RE.setScaleY((float) y_cordinateRange);
            txt_MI.setScaleY((float) y_cordinateRange);
            txt_FA.setScaleY((float) y_cordinateRange);
            txt_SO.setScaleY((float) y_cordinateRange);
            txt_RA.setScaleY((float) y_cordinateRange);
            txt_SHI.setScaleY((float) y_cordinateRange);

            txt_DO.setTranslationX((float)x_basicUnit * 1);
            txt_RE.setTranslationX((float)x_basicUnit * 2);
            txt_MI.setTranslationX((float)x_basicUnit * 3);
            txt_FA.setTranslationX((float)x_basicUnit * 4);
            txt_SO.setTranslationX((float)x_basicUnit * 5);
            txt_RA.setTranslationX((float)x_basicUnit * 6);
            txt_SHI.setTranslationX((float)x_basicUnit * 7);
        }
    }
    */

    //アプリ起動、画面回転などによるアプリ画面生成時に、画面情報を取得する
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        //XMLで記述したLinearLayoutの情報を、java（このプログラム）で呼び出す
        linearLayout = (LinearLayout)findViewById(R.id.linearLayout);
        //幅を取得
        contentWidth = linearLayout.getWidth();
        //高さを取得
        contentHeight = linearLayout.getHeight();
        //端末のステータスバーの高さを取得
        statusBarHeight = getStatusBarHeight(this);
        //TODO:同じ値ではないか？
        if(vertical_or_horizontal == 1) {
            x_cordinateRange = contentWidth;
            y_cordinateRange = contentHeight - statusBarHeight;
            x_basicUnit = x_cordinateRange / 7;
            y_basicUnit = y_cordinateRange / 7;
        } else {
            x_cordinateRange = contentWidth;
            y_cordinateRange = contentHeight - statusBarHeight;
            x_basicUnit = x_cordinateRange / 7;
            y_basicUnit = y_cordinateRange / 7;
        }
    }

    //音声を出力するクラス。出力する音声のIDを指定して、音量、優先度、ループ、再生速度を引数に対応して変化させる
    public class SoundPlayer {
        public SoundPool pool;
        public int se_id, se_DO, se_RE, se_MI, se_FA, se_SO, se_RA, se_SHI = -1;
        public SoundPlayer(Context context) {
            final int SOUND_POOL_MAX = 6;
            if (Build.VERSION.SDK_INT >= 21) {
                AudioAttributes attr = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build();
                pool = new SoundPool.Builder()
                        .setAudioAttributes(attr)
                        .setMaxStreams(SOUND_POOL_MAX)
                        .build();
            }
            if (Build.VERSION.SDK_INT < 21) {
                pool = new SoundPool(SOUND_POOL_MAX, AudioManager.STREAM_MUSIC, 0);
            }
            se_DO = pool.load(getApplicationContext(), R.raw.se_do, 0);
            se_RE = pool.load(getApplicationContext(), R.raw.se_re, 0);
            se_MI = pool.load(getApplicationContext(), R.raw.se_mi, 0);
            se_FA = pool.load(getApplicationContext(), R.raw.se_fa, 0);
            se_SO = pool.load(getApplicationContext(), R.raw.se_so, 0);
            se_RA = pool.load(getApplicationContext(), R.raw.se_ra, 0);
            se_SHI = pool.load(getApplicationContext(), R.raw.se_shi, 0);
        }

        //ID、音量、優先度、ループ回数、再生速度、の引数に対応して音声を出力する。
        // 優先度とループは引数として取るが、今回のアプリでは不要なため、それらを使わずに固定値の0を用いる。
        public void playVariousSound(int se_id, float leftVolume, float rightVolume, int priority, int loop, float rate) {
            pool.play(se_id, leftVolume, rightVolume, 0, 0, rate);
        }

        //背景色を設定しているTextViewの幅を任意の一つだけ最大化する
        public void maximizeTextViewWidth(final TextView txt_sound){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            //android:layout_weightを最大値に設定。設定したTextViewだけが画面に表示される
            params.weight = 7;
            //全てのTextViewを一旦、最小化してから
            MinimumizeTextWidth();
            //任意のTextViewを一つだけ最大化
            txt_sound.setLayoutParams(params);
            //最大化をした時間を取得
            maximizeTextViewWidthTime = new Date().getTime();
            //最後に最大化をしてから１秒以上経過していれば、全てのTextViewの幅をデフォルト値に戻す
            setTextViewWidthToDefault();
        }

        //背景色を設定している全てのTextViewの幅をデフォルト値にする
        public void setTextViewWidthToDefault(){
            Runnable updateText = new Runnable(){
                public void run() {
                    //最後にTextViewを最大化してから0.9秒(900ミリ秒）以上経過していたら
                    if (new Date().getTime() > maximizeTextViewWidthTime + 900) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //android:layout_weightを1に設定。全てのTextViewが画面全てを埋めるように均等に表示される
                        params.weight = 1;
                        txt_DO.setLayoutParams(params);
                        txt_RE.setLayoutParams(params);
                        txt_MI.setLayoutParams(params);
                        txt_FA.setLayoutParams(params);
                        txt_SO.setLayoutParams(params);
                        txt_RA.setLayoutParams(params);
                        txt_SHI.setLayoutParams(params);
                    }
                }
            };
            //１秒(1000ミリ秒)後にupdateTextの処理を実行
            new Handler().postDelayed(updateText, 1000);
        }

        //背景色を設定している全てのTextViewの幅を最小化する
        public void MinimumizeTextWidth(){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            //android:layout_weightを最小に設定。全てのTextViewが非表示になる
            params.weight = 0;
            txt_DO.setLayoutParams(params);
            txt_RE.setLayoutParams(params);
            txt_MI.setLayoutParams(params);
            txt_FA.setLayoutParams(params);
            txt_SO.setLayoutParams(params);
            txt_RA.setLayoutParams(params);
            txt_SHI.setLayoutParams(params);
        }

        //画面の座標に応じて、出力する音源のIDを取得する
        public int getSoundIdByCoordinate(float start_x, float start_y) {
            if(vertical_or_horizontal == 1){
                if (start_x <= x_basicUnit * 1) {
                    Log.d("sound", "DO");
                    //ついでに背景を最大化。本当は別のメソッドを作るべき
                    maximizeTextViewWidth(txt_DO);
                    return se_DO;
                } else if (start_x > x_basicUnit * 1 && start_x <= x_basicUnit * 2) {
                    Log.d("sound", "RE");
                    maximizeTextViewWidth(txt_RE);
                    return se_RE;
                } else if (start_x > x_basicUnit * 2 && start_x <= x_basicUnit * 3) {
                    Log.d("sound", "MI");
                    maximizeTextViewWidth(txt_MI);
                    return se_MI;
                } else if (start_x > x_basicUnit * 3 && start_x <= x_basicUnit * 4) {
                    Log.d("sound", "FA");
                    maximizeTextViewWidth(txt_FA);
                    return se_FA;
                } else if (start_x > x_basicUnit * 4 && start_x <= x_basicUnit * 5) {
                    Log.d("sound", "SO");
                    maximizeTextViewWidth(txt_SO);
                    return se_SO;
                } else if (start_x > x_basicUnit * 5 && start_x <= x_basicUnit * 6) {
                    Log.d("sound", "RA");
                    maximizeTextViewWidth(txt_RA);
                    return se_RA;
                } else if (start_x > x_basicUnit * 6 && start_x <= x_basicUnit * 7) {
                    Log.d("sound", "SHI");
                    maximizeTextViewWidth(txt_SHI);
                    return se_SHI;
                } else {
                    return -1;
                }
            } else if(vertical_or_horizontal == 2){
                start_y = start_y - statusBarHeight;
                if (start_y <= y_basicUnit * 1) {
                    Log.d("sound", "DO");
                    return se_DO;
                } else if (start_y > y_basicUnit * 1 && start_y <= y_basicUnit * 2) {
                    Log.d("sound", "RE");
                    return se_RE;
                } else if (start_y > y_basicUnit * 2 && start_y <= y_basicUnit * 3) {
                    Log.d("sound", "MI");
                    return se_MI;
                } else if (start_y > y_basicUnit * 3 && start_y <= y_basicUnit * 4) {
                    Log.d("sound", "FA");
                    return se_FA;
                } else if (start_y > y_basicUnit * 4 && start_y <= y_basicUnit * 5) {
                    Log.d("sound", "SO");
                    return se_SO;
                } else if (start_y > y_basicUnit * 5 && start_y <= y_basicUnit * 6) {
                    Log.d("sound", "RA");
                    return se_RA;
                } else if (start_y > y_basicUnit * 6 && start_y <= y_basicUnit * 7) {
                    Log.d("sound", "SHI");
                    return se_SHI;
                } else {
                    return -1;
                }
            }
            Log.d("contentWidth", String.valueOf(contentWidth));
            Log.d("contentHeight", String.valueOf(contentHeight));
            return -1;
        }

        //フリックをした距離に対応して、再生速度を取得する
        public float getRateByDistance(float distance_both) {
            //基準値を座標領域の大きさに合わせる
            double basic_distance;
            distance_x = Math.abs(start_x - finish_x);
            distance_y = Math.abs(start_y - finish_y);
            distance_both = distance_x + distance_y;
            if (distance_both < contentWidth) {
                basic_distance = distance_both / x_basicUnit;
            } else {
                basic_distance = distance_x / x_basicUnit;
            }
            Log.d("ログ", String.valueOf(basic_distance));
            //再生速度の数値の範囲は0.5~2.0。
            // 一切フリックをせずにタッチだけをした場合は、再生速度（再生時間）が最も早く(短く)なる
            float rate = (float) (0.5 + Math.floor(basic_distance) * 0.25);
            Log.d("ログ", String.valueOf(rate));
            return rate;

//            //最大値が1.5になるように、基準となる距離を取得
//            double basic_distance = (x_cordinateRange + y_cordinateRange) / 1.5;
//            //再生速度の数値の範囲は0.5~2.0。
//            // 一切フリックをせずにタッチだけをした場合は、再生速度（再生時間）が最も早く(短く)なる
//            float rate = (float) (2.0 - (distance_both / basic_distance));
//            return rate;
        }

        //フリックをした速度から、音量を取得する
        public float getSoundVolumeBySpeed(float distance_both, float elapsedTime) {
            //距離 ÷ 時間で速度を取得
            float speed = distance_both / elapsedTime;
            //速度から音量を取得。音量の数値の範囲は0~1。どれくらいの速度でどれくらいの音量を出すかという基準は適当
            float soundVolume = (float) 0.2 + speed / 10;
            Log.d("soundVolume", String.valueOf(soundVolume));
            return soundVolume;
        }
    }

    //ユーザーのタッチとフリックの操作を判定し、それに対応した処理をする
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            //タッチをした場合
            case MotionEvent.ACTION_DOWN:
                //横と縦の座標を取得
                start_x = (float) event.getX();
                start_y = (float) event.getY();
                Log.d("start_x", start_x + " start_y : " + start_y);
                //タッチをした時間を取得
                startTime = new Date().getTime();
                //タッチをした座標に対応して、音源のIDを取得
                sound_id = sp.getSoundIdByCoordinate(start_x, start_y);
                //デバッグモード用のテキストの内容を設定
                dbgtxt.setText("X = " + start_x + "   Y = " + start_y);
                break;
            //フリックをしている場合
            case MotionEvent.ACTION_MOVE:
                //座標を取得
                move_x = (float) event.getX();
                move_y = (float) event.getY();
                //デバッグモード用のテキストの内容を設定
                dbgtxt.setText("X = " + move_x + "   Y = " + move_y);
                break;
            //タッチorフリックが終わり、画面から指を離した場合
            case MotionEvent.ACTION_UP:
                //座標を取得
                finish_x = (float) event.getX();
                finish_y = (float) event.getY();
                Log.d("finish_x", finish_x + " finish_y : " + finish_y);
                //ACTION_DOUN時に取得した座標から、ACTION_UPの座標を引き、それを絶対値化してフリックの距離を取得
                distance_x = Math.abs(start_x - finish_x);
                distance_y = Math.abs(start_y - finish_y);
                distance_both = distance_x + distance_y;
                //タッチorフリックが終わった時間を取得
                finishTime = new Date().getTime();
                //タッチorフリックをしていた経過時間を取得
                elapsedTime = finishTime - startTime;
                Log.d("elapsedTime", String.valueOf(elapsedTime));
                //距離と経過時間から速さを割り出し、経過時間に対応して音量を取得
                soundVolume = sp.getSoundVolumeBySpeed(distance_both, elapsedTime);
                //距離から再生速度（音声再生時間）を取得
                rate = sp.getRateByDistance(distance_both);
                //音声を出す
                sp.playVariousSound(sound_id, soundVolume, soundVolume, 0, 0, rate);
                //デバッグモード用のテキストの内容を設定
                dbgtxt.setText("X = " + finish_x + "   Y = " + finish_y);
                break;
        }
        return true;
    }

    //オプションメニューが選択された場合の処理を決める
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.action_settings){
            return true;
        }
        //デバッグモードが選択された場合には、その表示、非表示を切り替える
        if(id == R.id.debug_mode){
            dbgtxt_vis_or_unvis = Math.abs(dbgtxt_vis_or_unvis - 1);
            if(dbgtxt_vis_or_unvis == 0){
                dbgtxt.setVisibility(View.INVISIBLE);
            }
            if(dbgtxt_vis_or_unvis == 1){
                dbgtxt.setVisibility(View.VISIBLE);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    //オプションメニューをアプリ内で表示させる
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //必要な情報を、画面の回転時、アプリの終了時などで失われないように保存する
    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        //デバッグモードが選択されているかどうかを取得する変数の値を保存
        outState.putInt("DBGTXT_VIS_OR_UNVIS", dbgtxt_vis_or_unvis);
    }

    //アプリが始まる直前の処理を決める。
    @Override
    protected void onResume() {
        super.onResume();
        //画面が横向きか縦向きかを判定し、画面の向きを変数に保存
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            vertical_or_horizontal = 1;
        } else if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            vertical_or_horizontal = 2;
        }
        //オプションメニューでデバッグモードが選択されていなければ非表示にし、選択されていれば表示する
        dbgtxt = (TextView) findViewById(R.id.debugText);
        if(dbgtxt_vis_or_unvis == 0){
            dbgtxt.setVisibility(View.INVISIBLE);
        }
        if(dbgtxt_vis_or_unvis == 1){
            dbgtxt.setVisibility(View.VISIBLE);
        }
    }

    //onSaveInstanceStateで保存した情報を引き出す
    @Override
    protected void onRestoreInstanceState(Bundle savedInstancesState){
        super.onRestoreInstanceState(savedInstancesState);
        if(savedInstancesState != null){
            //デバッグモードが選択されているかどうかを判定する変数の値を代入
            dbgtxt_vis_or_unvis = savedInstancesState.getInt("DBGTXT_VIS_OR_UNVIS");
        }
    }

}


